/**
 * Canvas based clone of the Quiet Place Project
 * At one point found at http://thequietplaceproject.com/thequietplace
 * 
 * All code written by Jake Joslyn
 * 
 * Copyright 2018 Jake Joslyn
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation 
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and 
 * to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of 
 * the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
 * SOFTWARE.
 */




//////////////////////////
/*-- GLOBAL VARIABLES --*/
//////////////////////////

let canvas = document.getElementById('canvas')          //  Canvas Element Reference
let ctx = canvas.getContext('2d')                       //  2D Context
let body = document.getElementsByTagName('body')        //  Body Reference

let input                                               //  Allow Input object ot be global
let scattered_letters = []                              //  Array of all current Scattered Letters Objects

//Set up canvas on page load
window.onload = function(){
    
    //Set the Canvas to be as large as the Window
    setCanvas(window.innerWidth, window.innerHeight)
    
    let input_set_x = canvas.width/2                    //  Text Input Width - set to be half the width of the window
    let input_set_y = canvas.height/2                   //  Text Input Height - set to be half the height of the window
    
    input = new thoughtInput(input_set_x, input_set_y)  //  Instantiate new 'Input'
    window.requestAnimationFrame(draw);                 //  Start Animating
    
}





///////////////////
/*-- FUNCTIONS --*/
///////////////////

/** @name draw
 * 
 *  @description Animation Loop Function
 */
function draw(){

    //Set background
    ctx.fillStyle = 'blue'
    ctx.fillRect(0,0, canvas.width, canvas.height)
    
    //Draw the input object
    input.draw(ctx)
    
    //Draw Scattered Letter Objects
    for(let x in scattered_letters){
        let this_sl = scattered_letters[x]
        this_sl.draw(ctx)
    }
    
    //Get next animation frame
    window.requestAnimationFrame(draw);
    
}



/** @name setCanvas
 * 
 *  @description Resets the Canvas dimensions
 *  @param {Number} wid - width to set the canvas to 
 *  @param {Number} hi - height to set the canvas to
 */
function setCanvas(wid, hi){
    
    canvas.width = wid;
    canvas.height = hi;
    
}





//////////////////////
/*-- CONSTRUCTORS --*/
//////////////////////

/** @class scatteredLetter
 *  
 *  @description Floating Letter Object Constructor 
 *  @param {String} letter - The letter value of this  
 *  @param {JSON} trajectory - JSON Object with x and y value representative of the 2-Dimensional trajectory vector of this letter
 *  @param {Number} x - Letter's x coordinate
 *  @param {Number} y - Letter's y coordinate
 * 
 *  @method draw(ctx) Draws the letter
 *      @param {JSON} ctx - 2D Context to draw to
 */
function scatteredLetter(letter, trajectory, x, y){
    
    this.letter = letter
    this.trajectory = trajectory
    this.x = x
    this.y = y
    
    this.draw = function(the_ctx){
        
        the_ctx.fillStyle = "#000"                      //  set letter's color
        the_ctx.font = '20px Courier New'               //  set letter's font
        the_ctx.fillText(this.letter, this.x, this.y)   //  draw letter
        
        this.x = this.x + trajectory.x                  //  update letter's x coordinate
        this.y = this.y + trajectory.y                  //  update letter's y coordinate

        // If the letter moves beyond the confines of the canvas
        if(this.x < 0 || this.x > canvas.width || this.y < 0 || this.y > canvas.height){
            
            //Find and delete this letter from scattered_letters array
            var index = scattered_letters.indexOf(this);
            if (index > -1) {
                scattered_letters.splice(index, 1);
            }
            
        }
        
    }
    
}


/** @class thoughtInput 
 * 
 *  @description Canvas simulation of text input element
 *  @param {Number} wid - Input Object's width
 *  @param {Number} hi - Input Object's height
 * 
 *  @method scatter() Scatters the letters currently in this input
 *  @method draw(ctx) Draws the Input box
 *      @param {JSON} ctx - 2D Context to draw to
 */
function thoughtInput(wid, hi){
    
    var self = this                                 //  Self-reference for use in event listeners
    this.width = wid                                //  Width of input
    this.height = hi                                //  Height of input
    this.text = ""                                  //  Current text in input
    this.focused = false                            //  Whether or not this object has focus for typing
    
    this.body_color = "#fff"                        //  Color of the background of the input
    this.stroke_color = "#0000ff"                   //  Color of the 'focus' stroke that appears when focus is on this input
    
    this.x = (canvas.width/2) - (this.width/2)      //  Top-Left x-coordinate
    this.y = (canvas.height/2) - (this.height/2)    //  Top-Left y-coordinate

    //  Handles mouse clicks
    canvas.addEventListener('mousedown', function(event){
        //If the click lands within the bounds of this object
        if(event.clientX > self.x && event.clientX < self.x + self.width && event.clientY > self.y && event.clientY < self.y  + self.height){
            self.focused = true     //  give this focus
        }else{  
            self.focused = false    //  remove focus
        } 
    })
    
    //  Handles keypresses
    canvas.addEventListener('keydown', function(event){
    
        //If this object has focus
        if(self.focused){
            
            //is the space bar
            if(event.keyCode == 32)
            {
                //scatter the letters
                self.scatter()

            //is delete key
            }else if(event.keyCode == 8){
                //Remove one letter from the text in this object
                self.text = self.text.substr(0, self.text.length - 1)
            }else if(event.key.length == 1){
                //Add letter to input
                self.text = self.text + event.key
            }
            
        }
        
    })
    
    
    this.scatter = function(){

        //For each letter in this input object
        for(let x in this.text){
            
            let letter = this.text[x]
            
            //give the letter an x trajectory between 1 and 4
            let x_vector_set = Math.floor(Math.random() * 4) + 1
            
            //assign a negative trajectory randomly
            if(Math.floor(Math.random() * 3) > 1){
                x_vector_set = x_vector_set * -1
            }
            
            //give the letter an y trajectory between 1 and 4
            let y_vector_set = Math.floor(Math.random() * 4) + 1
            
            //assign a negative trajectory randomly
            if(Math.floor(Math.random() * 3) > 1){
                y_vector_set = y_vector_set * -1
            }
            
            let trajectory = {x: x_vector_set, y: y_vector_set}
            
            //instantiate the new scatteredLetter
            let stardust = new scatteredLetter(letter, trajectory, this.x + (x * 20), this.y)
            //add new scatteredLetter to scattered_letters array
            scattered_letters.push(stardust)
            
        }

        //Clear out the text
        this.text = ""
    }
    
    this.draw = function(the_ctx){
        
        the_ctx.fillStyle = this.body_color                                             //  Set fill color
        the_ctx.fillRect(this.x, this.y, this.width, this.height)                       //  Draw input box
        
        the_ctx.fillStyle = "#000"                                                      //  Set letter color
        the_ctx.font = '20px Courier New'                                               //  Set letter font
        the_ctx.fillText(this.text.toString(), this.x + 4, this.y + 20, this.width - 8) //  Draw the letters
        
        if(this.focused){
            the_ctx.lineWidth = 5                                                           //  Set stroke width
            the_ctx.strokeStyle = this.stroke_color                                         //  Set stroke color
            the_ctx.strokeRect(this.x - 5, this.y - 5, this.width + 10, this.height + 10)   //  Draw Stroke
        }
        
    }
    
}